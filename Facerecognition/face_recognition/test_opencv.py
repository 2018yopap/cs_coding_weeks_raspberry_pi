from __future__ import print_function
import imutils
import cv2


def load_and_display_image(filename):
    image = cv2.imread(filename)
    cv2.imshow("Image", image)
    cv2.waitKey(0)
    return image




def extract_edges(filename):
    image = load_and_display_image(filename)

    cnts = cv2.findContours(image.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
    output = image.copy()

    # loop over the contour
    for c in cnts:
	# draw each contour on the output image with a 3px thick purple
	# outline, then display the output contours one at a time
	    cv2.drawContours(output, [c], -1, (240, 0, 159), 3)
	    cv2.imshow("Contours", output)
	    cv2.waitKey(0)

extract_edges("../Data/tetris_blocks.png")

