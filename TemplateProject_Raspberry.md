# Un système de reconnaissance faciale avec un Raspberry Pi



L'**objectif** de ce mini-projet est de developper, de manière très incrémentale, un système de reconnaissance faciale à l'aide d'un Raspberry Pi afin de vous former aux bonnes pratiques de la programmation et à la culture de la qualité logicielle. Vous découvriez aussi un ensemble d'outils pour faire de la vision par ordinateur et de l'apprentissage profond. Enfin, au travers de ce projet, vous decouvrirez plusieurs principes du mouvement dit du [*Software Craftmanship*](https://www.octo.com/fr/publications/20-culture-code). 

Ce projet vous permettre aussi de vous initier à la vision par ordinateur ainsi qu'aux nano-ordinateurs. Il est très largement inspiré des très bons tutorials de [Adrian Rosebrock](https://www.pyimagesearch.com/about/) sur ces différents sujets.


## A propos du Raspberry Pi

Le [Raspberry Pi](https://www.raspberrypi.org/) est un nano-ordinateur monocarte conçu par le créateur de jeux vidéo David Braben, dans le cadre de sa fondation Raspberry Pi qui vise à promouvoir les bases de la programmation dans les écoles et qui s'est très rapidement répandu et il existe donc de nombreux projets et très bons tutoriaux sur ce nano-ordinateur.

Quelques exemples de sites de projets autour du Raspberry Pi :

 + [https://projects.raspberrypi.org/en](https://projects.raspberrypi.org/en)
 + [https://www.hackster.io] (https://www.hackster.io/search?i=projects&q=Raspberry%20Pi)
 + ...


## A propos de la reconnaissance faciale

La [reconnaissance faciale](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_reconnaissance_faciale) est une tâche de reconnaissance visuelle qui consiste à identifier une personne à partir d'une image ou d'une vidéo de son visage. c'est une tâche qui a très longuement animée la communauté de la vision par ordinateur et pour laquelle nous avons aujourd'hui des méthodes très efficaces basées sur des techniques d'apprentissage profond et de transfert de connaissances. Vous pouvez lire rapidement ce [tutorial](https://towardsdatascience.com/face-recognition-for-beginners-a7a9bd5eb5c2) pour comprendre dans les grandes lignes comme cela marche ou [celui là](https://medium.com/@ageitgey/machine-learning-is-fun-part-4-modern-face-recognition-with-deep-learning-c3cffc121d78) qui est très bien aussi.



L'objectif de ce projet n'est pas de vous enseigner la vision par ordinateur et l'apprentissage profond mais de vous permettre de faire un projet de programmation de bout en bout et il faudra donc accepter d'utiliser des approches sans forcement les maîtriser. Nous nous baserons pour cela sur l'utilisation de plusieurs bibliothèques :

+ La bibliothèque [opencv](https://pypi.org/project/opencv-python/) pour la vision par ordinateur.
+ La bibliothèque [dlib](https://pypi.org/project/dlib/) pour l'apprentissage profond.
+ et d'autres...



## Organisation du mini-projet

Ce mini-projet est découpé en plusieurs objectifs, eux-même découpés en  **sprints** et **fonctionnalités**. La notion de sprint fait référence à la [méthode agile](https://fr.wikipedia.org/wiki/M%C3%A9thode_agile). Un sprint correspond à un intervalle de temps pendant lesquel l’équipe projet va compléter un certain nombre de tâches.

Ce travail de découpage a été fait pour vous mais c'est une des premières étapes à faire pour tout projet de developpement logiciel, au moins de manière macroscopique. Pensez-y la semaine prochaine !




### **Objectif 1 (MVP): Un système simple de reconnaissance de visage** (JOUR 1 et 2)

L'objectif de cette première journée est de constuire et d'implémenter une chaine algorithmique simple à l'aide de modules existants pour faire de la reconnaissance de visage (l'objectif principal de notre projet). Ce système pourrait être qualifié de **[MVP (Minimum Viable product)](https://medium.com/creative-wallonia-engine/un-mvp-nest-pas-une-version-simplifi%C3%A9e-de-votre-produit-89017ac748b0)** même s'il n'est pas porté sur Raspberry car il pourra permettre d'avoir des premiers retours utilisateurs par exemple.

Ce concept de MVP a été introduit par Eric Ries, l'auteur de [The Lean Startup](http://theleanstartup.com/), une approche spécifique du démarrage d'une activité économique et du lancement d'un produit. La figure ci-dessous permet de bien expliquer ce concept.

![MVP](./Images/mvp.png)


 + **Sprint 0** :
	 + [Installation du socle technique.](./Sprint0Install.md)
	 + [Analyse des besoins.](./Sprint0Analyse.md) 
	 + [Refexion autour de la conception.](./Sprint0Conception.md)

+ **Sprint 1 : Prise en main d'OpenCV**
 	+ [**Fonctionnalité 1** : Charger et afficher une image.](./Facerecognition_S1_displayimage.md)
 	+ [**Fonctionnalité 2** : Effectuer un traitement sur une image et afficher le résultat du traitement.](./Facerecognition_S1_traitement.md)

+ **Sprint 2 : Constitution d'une base de données de visages**
   + [**Fonctionnalité 3** : Structuration et constitution de la base de données](./Facerecognition_S2_database.md) 

 + **Sprint 3** : **Une module de reconnaissance visuelle par apprentissage profond**
 	+ [**Fonctionnalité 4** : Reconnaître un visage](./facepyrecognition.md)
 	+ [**Fonctionnalité 5** : Tester la reconnaissanace sur la base de données](./S3_testrecognition.md)


### **Objectif 2 : (MVP) Reconnaitre un visage avec un Raspberry Pi** (JOUR 3)

 + **Sprint 4 : Prise en main de votre Raspberry Pi**
 	+ [**Fonctionnalité 6** : Un Raspberry Pi en ordre de marche pour la reconnaissance visuelle](./Facerecognition_S4_rasp.md)
 	+ [**Fonctionnalité 7** : Effectuer une reconnaissance sur une image de votre base et afficher le résultat du traitement.](./Facerecognition_S4_traitement_pi.md)

### **Objectif 3 : Ajouter des utilisateurs : reconnaître les visages du groupe projet** (JOUR 4)
**<span style='color:red'>(A venir)</span>**

### **Objectif 4 : Et si on ajoutait un peu de reconnaissance vocale** (JOUR 4)
**<span style='color:red'>(A venir)</span>**



